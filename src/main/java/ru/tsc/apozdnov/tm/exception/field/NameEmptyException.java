package ru.tsc.apozdnov.tm.exception.field;

public final class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("FAULT!! Name is EMPTY");
    }

}
