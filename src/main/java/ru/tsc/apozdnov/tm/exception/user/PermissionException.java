package ru.tsc.apozdnov.tm.exception.user;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error!!! Permission close. Incorrect Role!");
    }

}
