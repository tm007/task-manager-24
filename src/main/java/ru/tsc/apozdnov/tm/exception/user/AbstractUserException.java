package ru.tsc.apozdnov.tm.exception.user;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public class AbstractUserException extends AbstractException {

    public AbstractUserException() {
        super();
    }

    public AbstractUserException(final String message) {
        super(message);
    }

    public AbstractUserException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(final Throwable cause) {
        super(cause);
    }

    protected AbstractUserException(final String message, final Throwable cause, final boolean enableSuppression, final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
