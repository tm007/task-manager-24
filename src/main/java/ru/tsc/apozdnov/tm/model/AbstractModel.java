package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class AbstractModel {

    @NotNull
    private String id = UUID.randomUUID().toString();

}
