package ru.tsc.apozdnov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.model.IWBS;
import ru.tsc.apozdnov.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private String projectId;

    @Nullable
    private Date dateCreated = new Date();

    @Nullable
    private Date dateBegin = new Date();

    @Nullable
    private Date dateEnd = new Date();

    public Task(
            @NotNull final String name,
            @NotNull final Status status,
            @NotNull final Date dateBegin) {
        this.name = name;
        this.status = status;
        this.dateBegin = dateBegin;
    }

    @Override
    public String toString() {
        return "NAME" + ": " + name + "  " +
                "DESCRIPTION: " + ": " + description + "  " +
                "PROJECTID:" + projectId + "\n";
    }

}
