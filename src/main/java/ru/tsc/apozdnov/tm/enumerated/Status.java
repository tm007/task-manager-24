package ru.tsc.apozdnov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.exception.system.EmptyStatusException;
import ru.tsc.apozdnov.tm.exception.system.IncorrectStatusException;

public enum Status {

    NOT_STARTED("Not started."),

    IN_PROGRESS("In progress"),

    COMPLETED("Completed");

    @NotNull
    private String displayName;

    @NotNull Status(String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(final Status status) {
        if (status == null) return "";
        return status.getDisplayName();
    }

    @NotNull
    public static Status toStatus(String val) {
        if (val == null || val.isEmpty()) throw new EmptyStatusException();
        for (Status status : values()) {
            if (status.name().equals(val)) return status;
        }
        throw new IncorrectStatusException(val);
    }

    public String getDisplayName() {
        return displayName;
    }
}
