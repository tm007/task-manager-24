package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.repository.IProjectRepository;
import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
import ru.tsc.apozdnov.tm.api.repository.IUserRepository;
import ru.tsc.apozdnov.tm.api.service.IUserService;
import ru.tsc.apozdnov.tm.enumerated.RoleType;
import ru.tsc.apozdnov.tm.exception.field.IdEmptyException;
import ru.tsc.apozdnov.tm.exception.user.*;
import ru.tsc.apozdnov.tm.model.User;
import ru.tsc.apozdnov.tm.util.HashUtil;

import java.util.Optional;

public class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public UserService(
            @NotNull final IUserRepository userRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository
    ) {
        super(userRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(login, password);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailExistException();
        return repository.create(login, password, email);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @Nullable final RoleType role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(login, password, role);
    }

    @NotNull
    @Override
    public User findOneByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findOneByLogin(login);
    }

    @NotNull
    @Override
    public User findOneByEmail(@NotNull final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        Optional<User> user = Optional.ofNullable(repository.findOneByEmail(email));
        return user.orElseThrow(UserIdEmptyException::new);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findOneByLogin(login);
        return remove(user);
    }

    @Nullable
    @Override
    public User removeByEmail(@NotNull final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findOneByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@NotNull final String id, @NotNull final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @NotNull
    @Override
    public User userUpdate(@NotNull final String id, @NotNull final String firstName, @NotNull final String lastName, @NotNull final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @NotNull
    @Override
    public boolean isLoginExist(@NotNull final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    public boolean isEmailExist(@NotNull final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @Override
    public void lockUserByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findOneByLogin(login);
        user.setLocked(true);
    }

    @NotNull
    @Override
    public void unlockUserByLogin(@NotNull final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findOneByLogin(login);
        user.setLocked(false);
    }

    @NotNull
    @Override
    public User remove(@NotNull final User model) {
        @NotNull final User user = super.remove(model);
        taskRepository.clear(user.getId());
        projectRepository.clear(user.getId());
        return user;
    }

}
