package ru.tsc.apozdnov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.util.ConvertByteUtil;

public final class InfoCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "info";

    @NotNull
    public static final String ARGUMENT = "-i";

    @NotNull
    public static final String DESCRIPTION = "Show System info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        final int availableprocessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableprocessors);
        final long freememory = runtime.freeMemory();
        @NotNull final String freeMemoryFormat = ConvertByteUtil.formatBytes(freememory);
        System.out.println("Free memory (bytes): " + freeMemoryFormat);
        long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = Long.toString(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormatValue = ConvertByteUtil.formatBytes(maxMemory);
        @NotNull final String maxMemoryFormat = (isMemoryLimit ? "no limit" : maxMemoryFormatValue);
        System.out.println("Maximum memory : " + maxMemoryFormat);
        final long totatMemory = runtime.totalMemory();
        final String totalMemoryFormat = ConvertByteUtil.formatBytes(totatMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long usedMemory = totatMemory - freememory;
        final String usedMemoryFormat = ConvertByteUtil.formatBytes(usedMemory);
        System.out.println("Used memory in JVM: " + usedMemoryFormat);
    }

}
