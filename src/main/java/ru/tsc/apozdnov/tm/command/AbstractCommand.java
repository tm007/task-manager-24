package ru.tsc.apozdnov.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.api.model.ICommand;
import ru.tsc.apozdnov.tm.api.service.IAuthService;
import ru.tsc.apozdnov.tm.api.service.IServiceLocator;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import javax.management.relation.Role;

public abstract class AbstractCommand implements ICommand {

    @NotNull
    protected IServiceLocator serviceLocator;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    @Nullable
    public abstract RoleType[] getRoleType();

    public abstract void execute();

    @Nullable
    public String getArgument() {
        return null;
    }

    @NotNull
    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public IAuthService getIAuthService() {
        return serviceLocator.getAuthService();
    }

    @NotNull
    public String getUserId() {
        return getIAuthService().getUserId();
    }

    @NotNull
    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    @Override
    public String toString() {
        @NotNull final String name = getName();
        @NotNull final String argument = getArgument();
        @NotNull final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        if (description != null && !description.isEmpty()) result += description;
        return result;
    }

}
