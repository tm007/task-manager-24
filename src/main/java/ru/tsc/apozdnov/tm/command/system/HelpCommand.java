package ru.tsc.apozdnov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.command.AbstractCommand;
import ru.tsc.apozdnov.tm.enumerated.RoleType;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display all commands";
    }

    @NotNull
    @Override
    public RoleType[] getRoleType() {
        return RoleType.values();
    }

    @NotNull
    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getTerminalCommand();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
